#!/usr/bin/env bash

if [[ "$1" = "" ]]; then
    mkdir ./build
    mkdir ./build/meucit.seubinknow.sbfs

    cp -a ./src/* ./build/meucit.seubinknow.sbfs
    cp -a ./seubin.meta/* ./build/meucit.seubinknow.sbfs

elif [[ "$1" = "wpkg" ]]; then

    [[ -d ./build ]] || ./build.sh

    WPKGNAME="meucit.seubinknow"
    WPKGVER=$(cat ./build/meucit.seubinknow.sbfs/book.json | jq -r '.version')

    mkdir ./wpkg-archive
    mkdir ./wpkg-archive/sbfs

    cp -a ./build/meucit.seubinknow.sbfs/* ./wpkg-archive/sbfs
    touch ./wpkg-archive/WHERE
    echo "things" > ./wpkg-archive/WHERE
    touch ./wpkg-archive/SBFSNAME
    echo "$WPKGNAME" > ./wpkg-archive/SBFSNAME
    cd ./wpkg-archive
    zip -r ../${WPKGNAME}@${WPKGVER}.wpkg ./
    cd ..
    rm -rf ./wpkg-archive

elif [[ "$1" = "clean" ]]; then
    rm -rf ./build
fi
