document.getElementById("theme").href="css/" + sbn.themePref() + ".css";

async function getTungJSON() {  // Get tung JSON
    try {
        const tjson = await fetch("./tungs/" + sbn.tungPref() + ".json");
        return await tjson.json();
    } catch (error) {
        const tjson = await fetch("./tungs/en.json"); //Download English json if the language's json cannot be downloaded
        return await tjson.json();
    }
}

async function bookMain() { // main function for localization and crap
        const tung = await getTungJSON();
        document.getElementById('seubinname').innerHTML = tung.sbnname + seubinver.name;
        document.getElementById('seubinver0').innerHTML = tung.sbnver + seubinver.ver;
        document.getElementById('seubinspec').innerHTML = tung.sbnspec + seubinver.spec;
        document.getElementById('seubinbootbook').innerHTML = tung.sbnbtbk + sbn.shellPref();
        document.getElementById('seubintung').innerHTML = tung.sbntung + sbn.tungPref();
        document.getElementById('browseragent').innerHTML = tung.brsragnt + navigator.userAgent;
        document.getElementById('seubintheme').innerHTML = tung.sbntheme + sbn.themePref();
        document.getElementById('seubiniconratio').innerHTML = tung.sbnicnrt + sbn.iconPref();
        document.getElementById('seubingender').innerHTML = tung.sbngndr + sbn.gender();
}

bookMain();

